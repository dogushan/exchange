package com.coin.exchange.model;

import com.coin.exchange.dto.ExchangeResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Coin implements Serializable {

    private String coin;
    private BigDecimal amount;
    private String currency;
    private BigDecimal currencyAmount;
    private boolean cached;

    public ExchangeResponse toExchangeResponse() {
        return ExchangeResponse.builder()
                .coin(coin)
                .currency(currency)
                .amount(amount)
                .currencyAmount(currencyAmount)
                .cached(cached)
                .build();

    }


}
