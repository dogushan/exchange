package com.coin.exchange.controller;

import com.coin.exchange.dto.ExchangeRequest;
import com.coin.exchange.dto.ExchangeResponse;
import com.coin.exchange.service.ICoinConverter;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.util.Optional;

@RestController
@RequestMapping("/api/exchange")
@RequiredArgsConstructor
public class ExchangeController {

    private final ICoinConverter<ExchangeRequest, ExchangeResponse> coinConverter;

    @GetMapping
    public ResponseEntity<ExchangeResponse> exchange(//@Valid ExchangeRequest exchangeRequest
            @RequestParam @NotBlank(message = "currency is mandatory")
            @Pattern(regexp = "^(USD|EUR)$") String currency,

            @RequestParam @DecimalMin(value = "0.0", inclusive = false)
            @Digits(integer = 4, fraction = 2)
            @Min(25)
            @Max(5000) BigDecimal currencyAmount,

            @RequestParam @NotBlank(message = "target coin is mandatory")
            @Pattern(regexp = "^(BTC|ETH)$") String targetCoin

            ) {

        ExchangeRequest exchangeRequest = ExchangeRequest.builder()
                .currencyAmount(currencyAmount)
                .currency(currency)
                .targetCoin(targetCoin)
                .build();

        ExchangeResponse exchangeResponse = coinConverter.apply(exchangeRequest);

        return ResponseEntity.of(Optional.of(exchangeResponse));
    }

}
