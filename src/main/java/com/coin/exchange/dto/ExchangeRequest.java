package com.coin.exchange.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

@Data
@Builder
public class ExchangeRequest {

    @NotBlank(message = "currency is mandatory")
    @Pattern(regexp = "^(USD|EUR)$")
    private String currency;

    @DecimalMin(value = "0.0", inclusive = false)
    @Digits(integer=4, fraction=2)
    @Min(25)
    @Max(5000)
    private BigDecimal currencyAmount;

    @NotBlank(message = "target coin is mandatory")
    @Pattern(regexp = "^(BTC|ETH)$")
    private String targetCoin;
}
