package com.coin.exchange.dto;

import com.coin.exchange.model.Coin;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class ExchangeResponse {
    private String coin;
    private BigDecimal amount;
    private String currency;
    private BigDecimal currencyAmount;
    private boolean cached;

    public Coin toCoin() {
        return Coin.builder()
                .coin(coin)
                .currency(currency)
                .currencyAmount(currencyAmount)
                .amount(amount)
                .cached(cached)
                .build();
    }

}
