package com.coin.exchange.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class BlockChainResponse {

    @JsonProperty("last_trade_price")
    private BigDecimal price;


/*
    {
        "symbol": "BTC-USD",
            "price_24h": 4998,
            "volume_24h": 0.3015,
            "last_trade_price": 5000
    }
    */

}
