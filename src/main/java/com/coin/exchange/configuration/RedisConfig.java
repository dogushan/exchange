package com.coin.exchange.configuration;

import com.coin.exchange.model.Coin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;

import org.springframework.data.redis.core.RedisTemplate;


@Configuration
public class RedisConfig {

    @Bean
    public RedisTemplate<String, Coin> redisTemplate(RedisConnectionFactory redisConnectionFactory){
        RedisTemplate<String, Coin> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory);
        return template;
    }

}
