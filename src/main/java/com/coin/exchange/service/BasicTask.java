package com.coin.exchange.service;

import java.util.function.Function;

public interface BasicTask<T,R> extends Function<T,R> {
}
