package com.coin.exchange.service;

import com.coin.exchange.dto.BlockChainResponse;
import com.coin.exchange.dto.ExchangeRequest;
import com.coin.exchange.dto.ExchangeResponse;
import com.coin.exchange.model.Coin;
import com.coin.exchange.repository.CoinRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.RoundingMode;
import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class CoinConverter implements ICoinConverter<ExchangeRequest, ExchangeResponse> {

    private final RestTemplate restTemplate;
    private final CoinRepository coinRepository;

    @Override
    public ExchangeResponse apply(ExchangeRequest exchangeRequest) {

        String key = generateRedisKey(exchangeRequest);
        Optional<Coin> maybeCoin = coinRepository.get(key);

        if (maybeCoin.isPresent()) {
            log.info("return from cache key : {}",key);
            Coin coin = maybeCoin.get();
            coin.setCached(true);
            return coin.toExchangeResponse();
        } else {
            log.info("return from api");
            String requestUrl = "https://api.blockchain.com/v3/exchange/tickers/";
            String symbol = exchangeRequest.getTargetCoin() + "-" + exchangeRequest.getCurrency();

            ResponseEntity<BlockChainResponse> responseResponseEntity = restTemplate.getForEntity(requestUrl + symbol, BlockChainResponse.class);
            BlockChainResponse blockChainResponse = responseResponseEntity.getBody();

            ExchangeResponse exchangeResponse = getExchangeResponse(exchangeRequest, blockChainResponse);

            Coin coin = exchangeResponse.toCoin();

            coinRepository.set(key, coin);

            return exchangeResponse;
        }

    }

    private ExchangeResponse getExchangeResponse(ExchangeRequest exchangeRequest, BlockChainResponse blockChainResponse) {
        return ExchangeResponse.builder()
                .coin(exchangeRequest.getTargetCoin())
                .currency(exchangeRequest.getCurrency())
                .amount(exchangeRequest.getCurrencyAmount().divide(blockChainResponse.getPrice(), 10, RoundingMode.CEILING))
                .currencyAmount(exchangeRequest.getCurrencyAmount())
                .cached(false)
                .build();
    }

    private String generateRedisKey(ExchangeRequest exchangeRequest) {
        return exchangeRequest.getCurrency() + "-" + exchangeRequest.getTargetCoin() + "-" + exchangeRequest.getCurrencyAmount();
    }

}
