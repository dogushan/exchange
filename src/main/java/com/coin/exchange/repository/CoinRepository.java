package com.coin.exchange.repository;

import com.coin.exchange.model.Coin;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.Optional;

@Repository
@AllArgsConstructor
public class CoinRepository {

    @Resource
    private final RedisTemplate<String, Coin> redisTemplate;

    public Optional<Coin> get(String key) {
        return Optional.ofNullable(redisTemplate.opsForValue().get(key));
    }

    public void set(String key, Coin coin) {
        redisTemplate.opsForValue().set(key, coin, Duration.ofSeconds(10));
    }

}
